oxidisched
==========

Batsim_-compatible schedulers mostly meant to test Batsim. Written in Rust_.

.. _Batsim: http://batsim.rtfd.io/
.. _Rust: https://www.rust-lang.org/
