{ nixpkgs-moz ? builtins.fetchGit {
    url = https://github.com/mozilla/nixpkgs-mozilla;
    rev = "e912ed483e980dfb4666ae0ed17845c4220e5e7c";
  }
, cargo2nix ? builtins.fetchGit {
    url = https://github.com/tenx-tech/cargo2nix;
    ref = "v0.8.2";
  }
, moz-overlay ? import "${nixpkgs-moz}/rust-overlay.nix"
, cargo2nix-overlay ? import "${cargo2nix}/overlay"
, pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.03.tar.gz")
    { overlays = [ moz-overlay cargo2nix-overlay ]; }
}:

let
  # rustPkgs = pkgs.rustBuilder.makePackageSet' {
  #     rustChannel = "stable";
  #     packageFun = import ./Cargo.nix;
  # };

  jobs = rec {
    inherit pkgs;

    dev-shell = pkgs.mkShell rec {
      name = "dev-shell";
      buildInputs = with pkgs; [
        rustc
        cargo
        pkgconfig
        zeromq
      ];
    };
    # wrappers = rustPkgs.workspace.oxidisched {};
  };
in
  jobs
