use std::path::PathBuf;
use std::fs::File;
use std::io::BufReader;

use anyhow::{bail, Result};
use structopt::StructOpt;
use serde_json;
use batsim_rust::{NetworkContext, run_whole_simulation};

use sched;

///////////////////////////////////////////////////////////

#[derive(StructOpt, Debug)]
#[structopt(name = "oxidisched", about = "Set of Batsim schedulers in Rust.")]
/// Command-Line Interface
struct Cli {
    #[structopt(short, long)]
    /// Lists available schedulers
    list_schedulers: bool,

    #[structopt(short, long, name = "sched-name")]
    /// Dumps specified scheduler's default configuration
    dump_config: Option<String>,

    /// Runs a scheduler as specified in the config file
    #[structopt(short, long, name = "file", parse(from_os_str))]
    config: Option<PathBuf>,
}

fn main() -> Result<()> {
    let args = Cli::from_args();

    if args.list_schedulers {
        let scheds = sched::list_scheds();
        println!("{}", scheds.join("\n"));
        return Ok(());
    }

    match args.dump_config {
        Some(sched_name) => {
            let config = sched::default_config(&sched_name)?;
            println!("{}", serde_json::to_string(&config)?);
            return Ok(());
        },
        None => {}
    }

    match args.config {
        Some(config_file) => {
            let file = File::open(config_file)?;
            let reader = BufReader::new(file);
            let content = serde_json::from_reader(reader)?;

            let config: sched::Config = serde_json::from_value(content)?;
            let mut sched = sched::new(config)?;

            let mut network = NetworkContext::new("tcp://*:28000")?;

            run_whole_simulation(&mut sched, &mut network)?;
            Ok(())
        }
        None => bail!("Nothing to do. Run with `--help` for usage.")
    }
}
