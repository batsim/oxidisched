//! Oxidisched configuration.
use serde_json::value::Value;
use serde::{Serialize, Deserialize};

///////////////////////////////////////////////////////////

#[derive(Debug, Serialize, Deserialize)]
/// An oxidisched configuration.
pub struct Config {
    /// The scheduler name.
    pub sched_name: String,
    /// The scheduler-specific configuration (JSON object).
    pub sched_config: Value,
}
