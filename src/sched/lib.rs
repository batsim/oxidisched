pub mod rejecter;
pub mod errors;
pub mod config;

pub use rejecter::*;
pub use errors::*;
pub use config::*;

use batsim_rust::Scheduler;

///////////////////////////////////////////////////////////

/// Instantiate a Scheduler from a descriptive Config.
pub fn new(config: Config) -> Result<Box<dyn Scheduler>, SchedulerInstantiationError> {
    let sched = match config.sched_name.as_str() {
        "rejecter" => rejecter::Rejecter::new(config.sched_config),
        x => return Err(errors::SchedulerInstantiationError::InvalidName(x.to_string()))
    };

    match sched {
        Err(x) => Err(x),
        Ok(x) => Ok(Box::new(x)),
    }
}

/// Returns the default Config of a scheduler.
pub fn default_config(sched_name: &str) -> Result<Config, SchedulerInstantiationError> {
    let config = match sched_name {
        "rejecter" => rejecter::Rejecter::default_config(),
        x => return Err(errors::SchedulerInstantiationError::InvalidName(x.to_string()))
    };

    Ok(Config {
        sched_name: sched_name.to_string(),
        sched_config: config,
    })
}

/// Lists all available schedulers.
pub fn list_scheds() -> Vec<&'static str> {
    vec![
        "rejecter",
    ]
}
