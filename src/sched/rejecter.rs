//! Scheduler that rejects all jobs.
use crate::errors::SchedulerInstantiationError;

use batsim_rust::*;
use serde_json::value::Value;
use serde_json::json;

///////////////////////////////////////////////////////////

pub struct Rejecter {}

impl Rejecter {
    pub fn new(_c: Value) -> Result<Self, SchedulerInstantiationError> {
        Ok(Rejecter {})
    }

    pub fn default_config() -> Value {
        json!({})
    }
}

impl Scheduler for Rejecter {
    fn take_decisions(&mut self, msg: &BatsimMessage) -> Result<Option<BatsimMessage>, Error> {
        let mut decisions = BatsimMessage::new(msg.now);

        for event in &msg.events {
            match event {
                BatsimEvent::JOB_SUBMITTED {timestamp: _, data} => decisions.events.push(BatsimEvent::REJECT_JOB {
                    timestamp: msg.now,
                    data: RejectJob { job_id: data.job_id.clone() }
                }),
                BatsimEvent::SIMULATION_ENDS {timestamp: _} => return Ok(None),
                _ => ()
            }
        }

        Ok(Some(decisions))
    }
}
