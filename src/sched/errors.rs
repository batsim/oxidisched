//! Oxidisched errors.
use thiserror::Error;

///////////////////////////////////////////////////////////

#[derive(Error, Debug)]
pub enum SchedulerInstantiationError {
    #[error("There is no scheduler named `{0}`")]
    InvalidName(String),
    #[error("Invalid config: {0}")]
    InvalidConfig(String),
}
